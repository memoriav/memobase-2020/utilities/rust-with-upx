FROM index.docker.io/library/rust:alpine AS upx-build
RUN apk update && apk add build-base git perl bash zlib-dev ucl-dev cmake
WORKDIR /buildenv
RUN git clone https://github.com/upx/upx.git && cd upx && git submodule update --init --recursive && make

FROM index.docker.io/library/rust:alpine
COPY --from=upx-build /buildenv/upx/build/release/upx /usr/bin/upx
RUN apk update && apk add musl-dev build-base cmake openssl-dev ucl
