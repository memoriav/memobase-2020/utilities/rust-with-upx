# Rust with UPX

Rust build environment to speed up the CI build process for Rust binaries.
Plus, it comes with the latest [UPX](https://github.com/upx/upx) dev version.
This circumvents a bug in the current stable version of UPX.
